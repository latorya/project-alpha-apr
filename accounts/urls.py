from django.urls import path
from accounts.views import login_view, logout_user, signup_view


urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup_view, name="signup"),
]
