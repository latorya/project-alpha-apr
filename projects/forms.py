from django import forms
from django.contrib.auth.models import User


class ProjectForm(forms.Form):
    name = forms.CharField(max_length=200)
    description = forms.CharField(widget=forms.Textarea)
    owner = forms.ModelChoiceField(queryset=User.objects.all())
